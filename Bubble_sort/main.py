import random

def generate_list():
    g_lista = []
    for i in range(10):
        k = random.randint(0, 100)
        g_lista.append(k)
    return g_lista


def sort_b(lista):
    q = 0
    w = 1
    n_lista = lista.copy()

    while (True):
        result = compare(lista[q], lista[w])
        lista[q] = result[0]
        lista[w] = result[1]

        q = w
        w = w + 1

        if w == len(lista):
            if n_lista == lista:
                break
            n_lista = lista.copy()
            q = 0
            w = 1

    return lista


def compare(x, y):
    if x < y:
        return x, y
    else:
        return y, x


g_lista = generate_list()
n_lista = g_lista.copy()
sort_b(n_lista)

print(f"Lista nieposortowana: {g_lista}")
print(f"Lista posortowana: {n_lista}")
