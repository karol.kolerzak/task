import tkinter as tk
import random

def generate_list():
    g_lista = []
    for i in range(10):
        k = random.randint(0, 100)
        g_lista.append(k)
    lbl_random_numbers["text"] = g_lista
    return g_lista

def sort_b():
    q = 0
    w = 1
    lista = []
    lista = list(lbl_random_numbers["text"].split(" "))
    lista = [int(x) for x in lista]
    n_lista = lista.copy()

    while(True):
        result = compare(lista[q], lista[w])
        lista[q] = result[0]
        lista[w] = result[1]

        q = w
        w = w + 1

        if w == len(lista):
            if n_lista == lista:
                break
            n_lista = lista.copy()
            q = 0
            w = 1

    lbl_sort_numbers["text"] = lista


def compare(x, y):
    if x < y:
        return x, y
    else:
        return y, x


window = tk.Tk()
window.title("Bubble Sort")
window.resizable(width=False, height=False)

frm_rn = tk.Frame(master=window)
btn_generate_numbers = tk.Button(master=frm_rn, text="GENERATE", command=generate_list, width=20)
lbl_random_numbers = tk.Label(master=frm_rn, text="Random Numbers")

frm_rn.grid(row=0, column=0, pady=20)
btn_generate_numbers.grid(row=0, column=0, padx=10, pady=20, sticky="w")
lbl_random_numbers.grid(row=0, column=1, padx=10, pady=20, sticky="e")

frm_sort = tk.Frame(master=window)
btn_sort = tk.Button(master=frm_sort, text="Sort", command=sort_b, width=20)
lbl_sort_numbers = tk.Label(master=frm_sort, text="NUMBERS")

frm_sort.grid(row=1, column=0, padx=10, pady=20, sticky='ew')
btn_sort.grid(row=0, column=0, padx=10, pady=20, sticky='w')
lbl_sort_numbers.grid(row=0, column=1, padx=10, pady=20, sticky='e')


window.mainloop()
