import socket
from _thread import *
import  sys

SERVER = "127.0.0.1"
PORT = 5555

FORMAT = "utf-8"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((SERVER, PORT))
except socket.error as e:
    str(e)

s.listen(2)
print("Waiting for a connection, Server started")

def threading_client(conn):
    conn.send(str.encode("connected"))
    reply = ""

    while True:
        try:
            data = conn.recv(2048)
            reply = data.decode(FORMAT)

            if not data:
                print("disconnected")
                break
            else:
                print("recived: ", reply)
                print("sending :", reply)

            conn.sendall(str.encode(reply))
        except:
            break





while True:
    conn, addr = s.accept()
    print("connected to: ", addr)

    start_new_thread(threading_client, (conn, ))