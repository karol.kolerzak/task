#1
users = [{"name": "Kamil", "country":"Poland"}, {"name":"John", "country": "USA"}, {"name": "Yeti"}]

def only_users_from_poland(users):
    new_users = []
    for user in users:
        if 'country' in user and user['country'] == "Poland":
            new_users.append(user)
    return new_users

def only_users_from_poland2(users):
    return [user for user in users if 'country' in user and user['country'] == "Poland"]

print(only_users_from_poland(users))
print(only_users_from_poland2(users))

##########################################################################
#2
# Display sum of first ten elements starting from element 5:

numbers = [1,5,2,3,1,4,1,23,12,2,3,1,2,31,23,1,2,3,1,23,1,2,3,123]
index = numbers.index(5)
result = sum(numbers[index: index + 10])
print(result)

#######################################################################################
#3

pow_list = [pow(x, 2) for x in range(1, 21)]
print(pow_list)

