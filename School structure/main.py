class Human:

    def __init__(self, name, surname, age, sex):
        self.name = name
        self.surname = surname
        self.age = age
        self.sex = sex


class Student(Human):

    def __init__(self, class_group, educator, timetable):
        self.class_group = class_group
        self.educator = educator
        self.timetable = timetable

    def change_class(self, new_class):
        self.class_group = new_class

    def change_timetable(self, new_timetable):
        self.timetable = new_timetable


class Employee(Human):

    def __init__(self, salary, start_date, account_number):
        self.salary = salary
        self.date_of_start_work = start_date
        self.account_number = account_number


class Teacher(Employee):

    def __init__(self, subject, student):
        self.subject = subject # list of subject
        self.student = student


class HeadTeacher(Employee):

    def change_salary(self, employee, new_salary):
        employee.salary = new_salary

    def add_new_employee(self):
        pass


class RepairMan(Employee):

    def repair_sth(self, something_to_do):
        pass

